cmake_minimum_required(VERSION 2.8)
project( smf-test-1 )

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )
find_package (Eigen3 REQUIRED )
include_directories( ${EIGEN3_INCLUDE_DIR} )
find_package( GTSAMCMakeTools )
find_package( GTSAM REQUIRED )
include_directories( ${GTSAM_INCLUDE_DIR} )

add_executable( sfm-test-1 sfm-test-1.cpp )
target_link_libraries( sfm-test-1 ${OpenCV_LIBS} )
target_link_libraries (sfm-test-1  Eigen3::Eigen)

add_executable( sfm-test-2 sfm-test-2.cpp )
target_link_libraries( sfm-test-2 ${OpenCV_LIBS} )
target_link_libraries ( sfm-test-2  Eigen3::Eigen )
target_link_libraries ( sfm-test-2  gtsam )